<?php

namespace Test\DataProvider;

use Samy\PhpUnit\AbstractDataProvider as PhpUnitAbstractDataProvider;

abstract class AbstractDataProvider extends PhpUnitAbstractDataProvider
{
    /**
     * Retrieve csv data.
     *
     * @param string $Name The CSV name.
     * @return array<array<string,string>>
     */
    public static function csv(string $Name): array
    {
        $filename = dirname(__DIR__) .
            DIRECTORY_SEPARATOR . "csv" .
            DIRECTORY_SEPARATOR . ltrim($Name, DIRECTORY_SEPARATOR) . ".csv";

        return parent::csv($filename);
    }

    /**
     * @param string $Name
     * @param string $Key
     * @return array<mixed>
     */
    protected static function jsonData(string $Name, string $Key): array
    {
        $filename = dirname(__DIR__) .
            DIRECTORY_SEPARATOR . "json" .
            DIRECTORY_SEPARATOR . ltrim($Name, DIRECTORY_SEPARATOR) . ".json";
        $json = self::json($filename);

        return is_array($json[$Key]) ? $json[$Key] : [];
    }
}
