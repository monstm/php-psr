<?php

namespace Test\DataProvider;

class UriDataProvider extends AbstractDataProvider
{
    /** @var string */
    private static $json_unittest = "uri";

    /**
     * @return array<mixed>
     */
    public static function dataScheme(): array
    {
        return self::jsonData(self::$json_unittest, "scheme");
    }

    /**
     * @return array<mixed>
     */
    public static function dataUserInfo(): array
    {
        return self::jsonData(self::$json_unittest, "user-info");
    }

    /**
     * @return array<mixed>
     */
    public static function dataHost(): array
    {
        return self::jsonData(self::$json_unittest, "host");
    }

    /**
     * @return array<mixed>
     */
    public static function dataPort(): array
    {
        return self::jsonData(self::$json_unittest, "port");
    }

    /**
     * @return array<mixed>
     */
    public static function dataPath(): array
    {
        return self::jsonData(self::$json_unittest, "path");
    }

    /**
     * @return array<mixed>
     */
    public static function dataQuery(): array
    {
        return self::jsonData(self::$json_unittest, "query");
    }

    /**
     * @return array<mixed>
     */
    public static function dataFragment(): array
    {
        return self::jsonData(self::$json_unittest, "fragment");
    }

    /**
     * @return array<mixed>
     */
    public static function dataString(): array
    {
        return self::jsonData(self::$json_unittest, "string");
    }
}
