<?php

namespace Test\DataProvider;

class ResponseDataProvider extends AbstractDataProvider
{
    /**
     * @return array<mixed>
     */
    public static function dataStatus(): array
    {
        $ret = []; // exception, code, reason

        foreach (self::csv("status") as $data) {
            $id = $data["id"];

            $ret[$id] = [[
                "exception" => $data["exception"] == "true",
                "code" => intval($data["code"]),
                "reason" => $data["reason"]
            ]];
        }

        return $ret;
    }
}
