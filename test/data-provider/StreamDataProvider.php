<?php

namespace Test\DataProvider;

class StreamDataProvider extends AbstractDataProvider
{
    /** @var string */
    private static $json_unittest = "stream";

    /**
     * @return array<mixed>
     */
    public static function dataFileNotExists(): array
    {
        return self::jsonData(self::$json_unittest, "file-not-exists");
    }

    /**
     * @return array<mixed>
     */
    public static function dataFileEmpty(): array
    {
        return self::jsonData(self::$json_unittest, "file-empty");
    }

    /**
     * @return array<mixed>
     */
    public static function dataFileExists(): array
    {
        return self::jsonData(self::$json_unittest, "file-exists");
    }

    /**
     * @return array<mixed>
     */
    public static function dataTemp(): array
    {
        return self::jsonData(self::$json_unittest, "temp");
    }

    /**
     * @return array<mixed>
     */
    public static function dataMemory(): array
    {
        return self::jsonData(self::$json_unittest, "memory");
    }
}
