<?php

namespace Test\DataProvider;

class MessageDataProvider extends AbstractDataProvider
{
    /** @var string */
    private static $json_unittest = "message";

    /**
     * @return array<mixed>
     */
    public static function dataProtocolVersion(): array
    {
        return self::jsonData(self::$json_unittest, "protocol-version");
    }

    /**
     * @return array<mixed>
     */
    public static function dataHeader(): array
    {
        return self::jsonData(self::$json_unittest, "header");
    }

    /**
     * @return array<mixed>
     */
    public static function dataFileExists(): array
    {
        return self::jsonData(self::$json_unittest, "file-exists");
    }

    /**
     * @return array<mixed>
     */
    public static function dataTemp(): array
    {
        return self::jsonData(self::$json_unittest, "temp");
    }

    /**
     * @return array<mixed>
     */
    public static function dataMemory(): array
    {
        return self::jsonData(self::$json_unittest, "memory");
    }
}
