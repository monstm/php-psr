<?php

namespace Test\DataProvider;

class ServerRequestDataProvider extends AbstractDataProvider
{
    /** @var string */
    private static $json_unittest = "server_request";

    /**
     * @return array<mixed>
     */
    public static function dataCookieParams(): array
    {
        return self::jsonData(self::$json_unittest, "cookie-params");
    }

    /**
     * @return array<mixed>
     */
    public static function dataQueryParams(): array
    {
        return self::jsonData(self::$json_unittest, "query-params");
    }

    /**
     * @return array<mixed>
     */
    public static function dataUploadedFiles(): array
    {
        return self::jsonData(self::$json_unittest, "uploaded-files");
    }

    /**
     * @return array<mixed>
     */
    public static function dataParsedBody(): array
    {
        return self::jsonData(self::$json_unittest, "parsed-body");
    }

    /**
     * @return array<mixed>
     */
    public static function dataAttribute(): array
    {
        return self::jsonData(self::$json_unittest, "attribute");
    }
}
