<?php

namespace Test\DataProvider;

class LoggerDataProvider extends AbstractDataProvider
{
    /** @var string */
    private static $json_unittest = "logger";

    /**
     * @return array<mixed>
     */
    public static function dataLog(): array
    {
        return self::jsonData(self::$json_unittest, "log");
    }
}
