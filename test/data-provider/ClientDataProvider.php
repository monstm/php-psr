<?php

namespace Test\DataProvider;

class ClientDataProvider extends AbstractDataProvider
{
    /** @var string */
    private static $json_unittest = "client";

    /**
     * @return array<mixed>
     */
    public static function dataRequest(): array
    {
        return self::jsonData(self::$json_unittest, "request");
    }
}
