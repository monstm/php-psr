<?php

namespace Test\DataProvider;

class RequestDataProvider extends AbstractDataProvider
{
    /** @var string */
    private static $json_unittest = "request";

    /**
     * @return array<mixed>
     */
    public static function dataMethod(): array
    {
        return self::jsonData(self::$json_unittest, "method");
    }
}
