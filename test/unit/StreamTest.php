<?php

namespace Test\Unit;

use InvalidArgumentException;
use RuntimeException;
use Samy\PhpUnit\AbstractTestCase;
use Samy\Psr7\Stream;

class StreamTest extends AbstractTestCase
{
    /** @var Stream */
    private $stream;

    /** @var array<string> */
    private static $cache_files = [];

    /**
     * @param string $Filename
     * @return void
     */
    public static function cacheFile($Filename): void
    {
        array_push(self::$cache_files, $Filename);
    }

    public static function tearDownAfterClass(): void
    {
        foreach (self::$cache_files as $cache_file) {
            if (is_file($cache_file)) {
                unlink($cache_file);
            }
        }
    }

    protected function setUp(): void
    {
        $this->stream = new Stream();
    }

    protected function tearDown(): void
    {
        $this->stream->close();
    }

    /**
     * @dataProvider \Test\DataProvider\StreamDataProvider::dataFileNotExists
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testFileNotExists(array $Data): void
    {
        $filename = "stream-not-exists-" . $Data["mode"] . "-" . sha1(strval(rand())) . ".temp";
        if (is_file($filename)) {
            $this->markTestSkipped($filename . ' already exists');
        }

        self::cacheFile($filename);

        if ($Data["exception"]) {
            $this->expectException(InvalidArgumentException::class);
        }
        /** @phpstan-ignore-next-line */
        $this->stream->withFile($filename, $Data["mode"]);
        $this->assertStream($this->stream, $Data);
    }

    /**
     * @dataProvider \Test\DataProvider\StreamDataProvider::dataFileEmpty
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testFileEmpty(array $Data): void
    {
        $filename = "stream-empty-" . $Data["mode"] . "-" . sha1(strval(rand())) . ".temp";
        file_put_contents($filename, "");
        self::cacheFile($filename);

        if ($Data["exception"]) {
            $this->expectException(InvalidArgumentException::class);
        }
        /** @phpstan-ignore-next-line */
        $this->stream->withFile($filename, $Data["mode"]);
        $this->assertStream($this->stream, $Data);
    }

    /**
     * @dataProvider \Test\DataProvider\StreamDataProvider::dataFileExists
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testFileExists(array $Data): void
    {
        $filename = "stream-empty-" . $Data["mode"] . "-" . sha1(strval(rand())) . ".temp";
        file_put_contents($filename, $Data["exists_content"]);
        self::cacheFile($filename);

        if ($Data["exception"]) {
            $this->expectException(InvalidArgumentException::class);
        }
        /** @phpstan-ignore-next-line */
        $this->stream->withFile($filename, $Data["mode"]);
        $this->assertStream($this->stream, $Data);
    }

    /**
     * @dataProvider \Test\DataProvider\StreamDataProvider::dataTemp
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testTemp(array $Data): void
    {
        /** @phpstan-ignore-next-line */
        $this->stream->withTemp($Data["mode"]);
        $this->assertStream($this->stream, $Data);
    }

    /**
     * @dataProvider \Test\DataProvider\StreamDataProvider::dataMemory
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testMemory(array $Data): void
    {
        /** @phpstan-ignore-next-line */
        $this->stream->withMemory($Data["mode"]);
        $this->assertStream($this->stream, $Data);
    }

    /**
     * @param Stream $Stream
     * @param array<string,mixed> $Data
     * @return void
     */
    private function assertStream(Stream $Stream, array $Data): void
    {
        $this->assertStreamCompatibility($Stream, $Data);
        $this->assertStreamResource(
            $Stream,
            /** @phpstan-ignore-next-line */
            $Data["init_size"],
            /** @phpstan-ignore-next-line */
            $Data["init_tell"],
            /** @phpstan-ignore-next-line */
            $Data["init_eof"],
            /** @phpstan-ignore-next-line */
            $Data["init_content"]
        );
        $this->assertStreamWrite($Stream, $Data);
        $this->assertStreamAppend($Stream, $Data);
        $this->assertStreamSeek($Stream, $Data);
        $this->assertStreamRead($Stream, $Data);
    }

    /**
     * @param Stream $Stream
     * @param array<string,mixed> $Data
     * @return void
     */
    private function assertStreamCompatibility(Stream $Stream, array $Data): void
    {
        $this->assertSame($Data["is_seekable"], $Stream->isSeekable());
        $this->assertSame($Data["is_readable"], $Stream->IsReadable());
        $this->assertSame($Data["is_writable"], $Stream->isWritable());
    }

    /**
     * @param Stream $Stream
     * @param int $Size
     * @param int $Tell
     * @param bool $Eof
     * @param string $Content
     * @return void
     */
    private function assertStreamResource(Stream $Stream, int $Size, int $Tell, bool $Eof, string $Content): void
    {
        $this->assertSame($Size, $Stream->getSize());

        if (!$Stream->isSeekable()) {
            $this->expectException(RuntimeException::class);
        }
        $this->assertSame($Tell, $Stream->tell());
        $this->assertSame($Eof, $Stream->eof());

        if (!$Stream->isSeekable()) {
            $this->expectException(RuntimeException::class);
        }
        $this->assertInstanceOf(Stream::class, $Stream->rewind());

        if (!$Stream->IsReadable()) {
            $this->expectException(RuntimeException::class);
        }
        $this->assertSame($Content, $Stream->getContents());
    }

    /**
     * @param Stream $Stream
     * @param array<string,mixed> $Data
     * @return void
     */
    private function assertStreamWrite(Stream $Stream, array $Data): void
    {
        if (!$Stream->isWritable()) {
            $this->expectException(RuntimeException::class);
        }
        /** @phpstan-ignore-next-line */
        $this->assertSame(strlen($Data["write_content"]), $Stream->write($Data["write_content"]));
        $this->assertStreamResource(
            $Stream,
            /** @phpstan-ignore-next-line */
            $Data["size_after_write"],
            /** @phpstan-ignore-next-line */
            $Data["tell_after_write"],
            /** @phpstan-ignore-next-line */
            $Data["eof_after_write"],
            /** @phpstan-ignore-next-line */
            $Data["content_after_write"]
        );
    }

    /**
     * @param Stream $Stream
     * @param array<string,mixed> $Data
     * @return void
     */
    private function assertStreamAppend(Stream $Stream, array $Data): void
    {
        if (!$Stream->isWritable()) {
            $this->expectException(RuntimeException::class);
        }
        /** @phpstan-ignore-next-line */
        $this->assertSame(strlen($Data["append_content"]), $Stream->write($Data["append_content"]));
        $this->assertStreamResource(
            $Stream,
            /** @phpstan-ignore-next-line */
            $Data["size_after_append"],
            /** @phpstan-ignore-next-line */
            $Data["tell_after_append"],
            /** @phpstan-ignore-next-line */
            $Data["eof_after_append"],
            /** @phpstan-ignore-next-line */
            $Data["content_after_append"]
        );
    }

    /**
     * @param Stream $Stream
     * @param array<string,mixed> $Data
     * @return void
     */
    private function assertStreamSeek(Stream $Stream, array $Data): void
    {
        if (!$Stream->isSeekable()) {
            $this->expectException(RuntimeException::class);
        }
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Stream::class, $Stream->seek($Data["seek_offset"]));

        if (!$Stream->isWritable()) {
            $this->expectException(RuntimeException::class);
        }
        /** @phpstan-ignore-next-line */
        $this->assertSame(strlen($Data["seek_content"]), $Stream->write($Data["seek_content"]));
        $this->assertStreamResource(
            $Stream,
            /** @phpstan-ignore-next-line */
            $Data["size_after_seek"],
            /** @phpstan-ignore-next-line */
            $Data["tell_after_seek"],
            /** @phpstan-ignore-next-line */
            $Data["eof_after_seek"],
            /** @phpstan-ignore-next-line */
            $Data["content_after_seek"]
        );
    }

    /**
     * @param Stream $Stream
     * @param array<string,mixed> $Data
     * @return void
     */
    private function assertStreamRead(Stream $Stream, array $Data): void
    {
        if (!$Stream->isSeekable()) {
            $this->expectException(RuntimeException::class);
        }
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Stream::class, $Stream->seek($Data["read_offset"]));

        if (!$Stream->IsReadable()) {
            $this->expectException(RuntimeException::class);
        }
        /** @phpstan-ignore-next-line */
        $this->assertSame($Data["read_content"], $Stream->read($Data["read_length"]));

        if (!$Stream->isSeekable()) {
            $this->expectException(RuntimeException::class);
        }
        $this->assertSame($Data["tell_after_read"], $Stream->tell());

        if (!$Stream->IsReadable()) {
            $this->expectException(RuntimeException::class);
        }
        $this->assertSame($Data["content_after_read"], $Stream->getContents());
    }
}
