<?php

namespace Test\Unit;

use Samy\PhpUnit\AbstractTestCase;
use Test\Inherit\Message;
use TypeError;

class MessageTest extends AbstractTestCase
{
    /** @var Message */
    private $message;

    protected function setUp(): void
    {
        $this->message = new Message();
    }

    /**
     * @return void
     */
    public function testDefault(): void
    {
        $this->assertSame("", $this->message->getProtocolVersion());
        $this->assertSame([], $this->message->getHeaders());
        $this->assertSame(false, $this->message->hasHeader("non-exists"));
        $this->assertSame([], $this->message->getHeader("non-exists"));
        $this->assertSame("", $this->message->getHeaderLine("non-exists"));
        $this->assertInstanceOf("Psr\Http\Message\StreamInterface", $this->message->getBody());
    }

    /**
     * @dataProvider \Test\DataProvider\MessageDataProvider::dataProtocolVersion
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testProtocolVersion(array $Data): void
    {
        /** @phpstan-ignore-next-line */
        $this->message->withProtocolVersion($Data["version"]);
        $this->assertSame($Data["version"], $this->message->getProtocolVersion());
    }

    /**
     * @dataProvider \Test\DataProvider\MessageDataProvider::dataHeader
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testHeader(array $Data): void
    {
        if ($Data["exception"]) {
            $this->expectException(TypeError::class);
        }

        /** @phpstan-ignore-next-line */
        $this->assertFalse($this->message->hasHeader($Data["name"]));
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Message::class, $this->message->withHeader($Data["name"], $Data["value"]));
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Message::class, $this->message->withAddedHeader($Data["name"], $Data["added"]));
        /** @phpstan-ignore-next-line */
        $this->assertTrue($this->message->hasHeader($Data["name"]));
        /** @phpstan-ignore-next-line */
        $this->assertSame($Data["header"], $this->message->getHeader($Data["name"]));
        /** @phpstan-ignore-next-line */
        $this->assertSame($Data["headerline"], $this->message->getHeaderLine($Data["name"]));
        $this->assertSame($Data["headers"], $this->message->getHeaders());
    }

    /**
     * @return void
     */
    public function testBody(): void
    {
        $text = "the quick brown fox jumps over the lazy dog";
        $body = $this->message->getBody();
        $body->write($text);

        $this->assertSame($text, $this->message->getBody()->__toString());
    }
}
