<?php

namespace Test\Unit;

use InvalidArgumentException;
use Samy\PhpUnit\AbstractTestCase;
use Samy\Psr7\Uri;

class UriTest extends AbstractTestCase
{
    /** @var Uri */
    private $uri;

    protected function setUp(): void
    {
        $this->uri = new Uri();
    }

    /**
     * @return void
     */
    public function testDefault(): void
    {
        $this->assertSame("", $this->uri->getScheme());
        $this->assertSame("", $this->uri->getAuthority());
        $this->assertSame("", $this->uri->getUserInfo());
        $this->assertSame("", $this->uri->getHost());
        $this->assertSame(null, $this->uri->getPort());
        $this->assertSame("", $this->uri->getPath());
        $this->assertSame("", $this->uri->getQuery());
        $this->assertSame("", $this->uri->getFragment());
    }

    /**
     * @dataProvider \Test\DataProvider\UriDataProvider::dataScheme
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testScheme(array $Data): void
    {
        if ($Data["exception"]) {
            $this->expectException(InvalidArgumentException::class);
        }

        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Uri::class, $this->uri->withScheme($Data["scheme"]));
        $this->assertSame($Data["scheme"], $this->uri->getScheme());
    }

    /**
     * @dataProvider \Test\DataProvider\UriDataProvider::dataUserInfo
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testUserInfo(array $Data): void
    {
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Uri::class, $this->uri->withUserInfo($Data["user"], $Data["password"]));
        $this->assertSame($Data["expect"], $this->uri->getUserInfo());
    }

    /**
     * @dataProvider \Test\DataProvider\UriDataProvider::dataHost
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testHost(array $Data): void
    {
        if ($Data["exception"]) {
            $this->expectException(InvalidArgumentException::class);
        }

        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Uri::class, $this->uri->withHost($Data["host"]));
        $this->assertSame($Data["host"], $this->uri->getHost());
    }

    /**
     * @dataProvider \Test\DataProvider\UriDataProvider::dataPort
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testPort(array $Data): void
    {
        if ($Data["exception"]) {
            $this->expectException(InvalidArgumentException::class);
        }

        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Uri::class, $this->uri->withPort($Data["port"]));
        $this->assertSame($Data["port"], $this->uri->getPort());
    }

    /**
     * @dataProvider \Test\DataProvider\UriDataProvider::dataPath
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testPath(array $Data): void
    {
        if ($Data["exception"]) {
            $this->expectException(InvalidArgumentException::class);
        }

        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Uri::class, $this->uri->withPath($Data["path"]));
        $this->assertSame($Data["path"], $this->uri->getPath());
    }

    /**
     * @dataProvider \Test\DataProvider\UriDataProvider::dataQuery
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testQuery(array $Data): void
    {
        if ($Data["exception"]) {
            $this->expectException(InvalidArgumentException::class);
        }

        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Uri::class, $this->uri->withQuery($Data["query"]));
        $this->assertSame($Data["expect"], $this->uri->getQuery());
    }

    /**
     * @dataProvider \Test\DataProvider\UriDataProvider::dataFragment
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testFragment(array $Data): void
    {
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Uri::class, $this->uri->withFragment($Data["fragment"]));
        $this->assertSame($Data["expect"], $this->uri->getFragment());
    }

    /**
     * @dataProvider \Test\DataProvider\UriDataProvider::dataString
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testString(array $Data): void
    {
        if ($Data["exception"]) {
            $this->expectException(InvalidArgumentException::class);
        }

        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Uri::class, $this->uri->withScheme($Data["scheme"]));
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Uri::class, $this->uri->withUserInfo($Data["user"], $Data["password"]));
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Uri::class, $this->uri->withHost($Data["host"]));
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Uri::class, $this->uri->withPort($Data["port"]));
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Uri::class, $this->uri->withPath($Data["path"]));
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Uri::class, $this->uri->withQuery($Data["query"]));
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Uri::class, $this->uri->withFragment($Data["fragment"]));
        $this->assertSame($Data["expect"], $this->uri->__toString());
    }
}
