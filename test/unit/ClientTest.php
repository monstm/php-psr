<?php

namespace Test\Unit;

use Samy\PhpUnit\AbstractTestCase;
use Samy\Psr18\Client;
use Samy\Psr7\Request;

class ClientTest extends AbstractTestCase
{
    /** @var Client */
    private $client;

    protected function setUp(): void
    {
        $this->client = new Client();
    }

    /**
     * @dataProvider \Test\DataProvider\ClientDataProvider::dataRequest
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testRequest(array $Data): void
    {
        $request = new Request();
        $request
            /** @phpstan-ignore-next-line */
            ->withMethod($Data["method"])
            ->withRequestTarget($Data["target"]);

        $response = $this->client->sendRequest($request);
        $this->assertSame($Data["protocol-version"], $response->getProtocolVersion());
        $this->assertSame($Data["status-code"], $response->getStatusCode());
        $this->assertSame($Data["status-reason"], $response->getReasonPhrase());
        $this->assertSame($Data["content-type"], $response->getContentType());

        $body = $response->getBody();
        $this->assertSame($Data["md5"], md5($body));
        $this->assertSame($Data["sha1"], sha1($body));
    }
}
