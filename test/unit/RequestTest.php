<?php

namespace Test\Unit;

use InvalidArgumentException;
use Samy\Dummy\Random;
use Samy\PhpUnit\AbstractTestCase;
use Samy\Psr7\Request;

class RequestTest extends AbstractTestCase
{
    /** @var Request */
    private $request;

    protected function setUp(): void
    {
        $this->request = new Request();
    }

    /**
     * @return void
     */
    public function testDefault(): void
    {
        $this->assertSame("/", $this->request->getRequestTarget());
        $this->assertSame("", $this->request->getMethod());
        $this->assertInstanceOf("Psr\Http\Message\UriInterface", $this->request->getUri());
    }

    /**
     * @return void
     */
    public function testRequestTarget(): void
    {
        $random = new Random();
        foreach (range(1, $random->integer(10, 50)) as $index) {
            $request = new Request();
            $url = $random->url();
            $this->assertInstanceOf(Request::class, $request->withRequestTarget($url));
            $this->assertSame($url, $request->getRequestTarget());
        }
    }

    /**
     * @dataProvider \Test\DataProvider\RequestDataProvider::dataMethod
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testMethod(array $Data): void
    {
        if ($Data["exception"]) {
            $this->expectException(InvalidArgumentException::class);
        }

        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Request::class, $this->request->withMethod($Data["method"]));
        $this->assertSame($Data["method"], $this->request->getMethod());
    }
}
