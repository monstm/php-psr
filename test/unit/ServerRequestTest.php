<?php

namespace Test\Unit;

use Samy\PhpUnit\AbstractTestCase;
use Samy\Psr7\ServerRequest;
use Samy\Psr7\UploadedFile;

class ServerRequestTest extends AbstractTestCase
{
    /** @var ServerRequest */
    private $server_request;

    protected function setUp(): void
    {
        $this->server_request = new ServerRequest();
    }

    /**
     * @return void
     */
    public function testDefault(): void
    {
        $this->assertSame([], $this->server_request->getCookieParams());
        $this->assertSame([], $this->server_request->getQueryParams());
        $this->assertSame([], $this->server_request->getUploadedFiles());
        $this->assertSame(null, $this->server_request->getParsedBody());
        $this->assertSame([
            "has_authorization" => false,
            "auth_type" => "",
            "auth_credentials" => "",
            "auth_username" => "",
            "auth_password" => "",
            "user_agent" => "",
            "referrer" => "",
            "remote_ip" => "0.0.0.0",
            "client_ip" => "0.0.0.0"
        ], $this->server_request->getAttributes());

        $this->assertSame(null, $this->server_request->getAttribute("non-exists"));
    }

    /**
     * @dataProvider \Test\DataProvider\ServerRequestDataProvider::dataCookieParams
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testCookieParams(array $Data): void
    {
        $this->assertInstanceOf(ServerRequest::class, $this->server_request->withCookieParams($Data));
        $this->assertSame($Data, $this->server_request->getCookieParams());
    }

    /**
     * @dataProvider \Test\DataProvider\ServerRequestDataProvider::dataQueryParams
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testQueryParams(array $Data): void
    {
        $this->assertInstanceOf(ServerRequest::class, $this->server_request->withQueryParams($Data));
        $this->assertSame($Data, $this->server_request->getQueryParams());
    }

    /**
     * @dataProvider \Test\DataProvider\ServerRequestDataProvider::dataUploadedFiles
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testUploadedFiles(array $Data): void
    {
        $uploaded_files = [];
        foreach ($Data as $name => $files) {
            $buffer = [];
            /** @phpstan-ignore-next-line */
            foreach ($files as $file) {
                /** @phpstan-ignore-next-line */
                array_push($buffer, new UploadedFile($file));
            }

            $uploaded_files[$name] = $buffer;
        }

        $this->assertInstanceOf(ServerRequest::class, $this->server_request->withUploadedFiles($uploaded_files));
        $this->assertSame($uploaded_files, $this->server_request->getUploadedFiles());
    }
}
