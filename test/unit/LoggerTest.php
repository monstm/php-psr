<?php

namespace Test\Unit;

use Samy\PhpUnit\AbstractTestCase;
use Samy\Psr3\Logger;

class LoggerTest extends AbstractTestCase
{
    /** @var Logger */
    private $logger;

    protected function setUp(): void
    {
        $this->logger = new Logger();
    }

    /**
     * @dataProvider \Test\DataProvider\LoggerDataProvider::dataLog
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testLog(array $Data): void
    {
        /** @phpstan-ignore-next-line */
        if (SKIP_LOG) {
            $this->markTestSkipped(__FUNCTION__ . ' mark as skip');
        }

        /** @phpstan-ignore-next-line */
        $this->logger->log($Data["level"], $Data["message"], $Data["context"]);
    }

    /**
     * @dataProvider \Test\DataProvider\LoggerDataProvider::dataLog
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testInfo(array $Data): void
    {
        /** @phpstan-ignore-next-line */
        if (SKIP_LOG) {
            $this->markTestSkipped(__FUNCTION__ . ' mark as skip');
        }

        /** @phpstan-ignore-next-line */
        $this->logger->info($Data["message"], $Data["context"]);
    }

    /**
     * @dataProvider \Test\DataProvider\LoggerDataProvider::dataLog
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testDebug(array $Data): void
    {
        /** @phpstan-ignore-next-line */
        if (SKIP_LOG) {
            $this->markTestSkipped(__FUNCTION__ . ' mark as skip');
        }

        /** @phpstan-ignore-next-line */
        $this->logger->debug($Data["message"], $Data["context"]);
    }
}
