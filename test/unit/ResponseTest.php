<?php

namespace Test\Unit;

use InvalidArgumentException;
use Samy\PhpUnit\AbstractTestCase;
use Samy\Psr7\Response;

class ResponseTest extends AbstractTestCase
{
    /** @var Response */
    private $response;

    protected function setUp(): void
    {
        $this->response = new Response();
    }

    /**
     * @return void
     */
    public function testDefault(): void
    {
        $this->assertResponse(0, "");
    }

    /**
     * @dataProvider \Test\DataProvider\ResponseDataProvider::dataStatus
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testStatusWithReason(array $Data): void
    {
        if ($Data["exception"]) {
            $this->expectException(InvalidArgumentException::class);
        }

        $reason = "status-" . strval($Data["code"]);
        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Response::class, $this->response->withStatus($Data["code"], $reason));
        /** @phpstan-ignore-next-line */
        $this->assertResponse($Data["code"], $reason);
    }

    /**
     * @dataProvider \Test\DataProvider\ResponseDataProvider::dataStatus
     * @param array<string,mixed> $Data
     * @return void
     */
    public function testStatusWithoutReason(array $Data): void
    {
        if ($Data["exception"]) {
            $this->expectException(InvalidArgumentException::class);
        }

        /** @phpstan-ignore-next-line */
        $this->assertInstanceOf(Response::class, $this->response->withStatus($Data["code"]));
        /** @phpstan-ignore-next-line */
        $this->assertResponse($Data["code"], $Data["reason"]);
    }

    /**
     * @param int $StatusCode
     * @param string $ReasonPhrase
     * @return void
     */
    private function assertResponse(int $StatusCode, string $ReasonPhrase): void
    {
        $this->assertSame($StatusCode, $this->response->getStatusCode());
        $this->assertSame($ReasonPhrase, $this->response->getReasonPhrase());
    }
}
