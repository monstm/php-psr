<?php

namespace Samy\Psr\DataProvider;

use Samy\DataProvider\DataProvider as DataProviderDataProvider;

class DataProvider extends DataProviderDataProvider
{
    /**
     * Retrieve JSon data provider.
     *
     * @param string $Name
     * @return array<mixed>
     */
    public static function json(string $Name): array
    {
        return parent::json(__DIR__ . DIRECTORY_SEPARATOR . ltrim($Name, DIRECTORY_SEPARATOR) . ".json");
    }
}
