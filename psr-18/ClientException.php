<?php

namespace Samy\Psr18;

use Exception;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Message\RequestInterface;
use Throwable;

/**
 * Every HTTP client related exception MUST implement this interface.
 */
class ClientException extends Exception implements ClientExceptionInterface
{
    /** @var RequestInterface */
    protected $request = null;

    /**
     * Client exception construction.
     *
     * @param RequestInterface $request Client request
     * @param string $message The exception message
     * @param int $code The exception code
     * @param ?Throwable $previous Previous throwable
     */
    public function __construct(
        RequestInterface $request,
        string $message = '',
        int $code = 0,
        ?Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);

        $this->request = $request;
    }
}
