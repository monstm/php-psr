<?php

namespace Samy\Psr18;

use Samy\Psr\Abstract\AbstractClient;

/**
 * Simple Client Implementation.
 */
class Client extends AbstractClient
{
    /**
     * Return an instance with the provided follow location.
     *
     * @param bool $FollowLocation
     * @return static
     */
    public function withFollowLocation(bool $FollowLocation): self
    {
        $this->follow_location = $FollowLocation;

        return $this;
    }

    /**
     * Return an instance with the provided ssl verification.
     *
     * @param bool $SslVerification
     * @return static
     */
    public function withSslVerification(bool $SslVerification): self
    {
        $this->ssl_verification = $SslVerification;

        return $this;
    }

    /**
     * Return an instance with the provided connect timeout.
     *
     * @param int $ConnectTimeout
     * @return static
     */
    public function withConnectTimeout(int $ConnectTimeout): self
    {
        $this->connect_timeout = $ConnectTimeout;

        return $this;
    }

    /**
     * Return an instance with the provided timeout.
     *
     * @param int $Timeout
     * @return static
     */
    public function withTimeout(int $Timeout): self
    {
        $this->timeout = $Timeout;

        return $this;
    }
}
