# PHP PSR

[
	![](https://badgen.net/packagist/v/samy/psr/latest)
	![](https://badgen.net/packagist/license/samy/psr)
	![](https://badgen.net/packagist/dt/samy/psr)
	![](https://badgen.net/packagist/favers/samy/psr)
](https://packagist.org/packages/samy/psr)

Moving PHP forward through collaboration and standards.

Welcome to the PHP Framework Interop Group!
We're a group of established PHP projects whose goal is to talk about commonalities between our projects and find ways we can work better together.

Please see [PHP-FIG](https://php-fig.org/) for more information.

---

## Implementations

| CODE   | TITLE                  | EDITOR                  | COORDINATOR   | SPONSOR       |
| :----- | :--------------------- | :---------------------- | :------------ | :------------ |
| PSR-3  | Logger Interface       | Jordi Boggiano          | N/A           | N/A           |
| PSR-7  | HTTP Message Interface | Matthew Weier O'Phinney | Beau Simensen | Paul M. Jones |
| PSR-18 | HTTP Client            | Tobias Nyholm           | N/A           | Sara Golemon  |

---

## Instalation

### Composer

Composer is a tool for dependency management in PHP.
It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.

```sh
composer require samy/psr
```

Please see [Composer](https://getcomposer.org/) for more information.

---

## Support

* Repository: <https://gitlab.com/monstm/php-psr>
* User Manual: <https://monstm.gitlab.io/php-psr/>
* Documentation: <https://monstm.alwaysdata.net/php-psr/>
* Issues: <https://gitlab.com/monstm/php-psr/-/issues>
