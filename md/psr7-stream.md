# Stream

Describes a data stream.

Typically, an instance will wrap a PHP stream; this interface provides
a wrapper around the most common operations, including serialization of
the entire stream to a string.

---

## Stream Instance

Simple Stream Implementation.

```php
$stream = new \Samy\Psr7\Stream();
```

---

## Stream Interface

Describes Stream interface.

### close

Closes the stream and any underlying resources.

```php
$stream->close();
```

### detach

Separates any underlying resources from the stream.
After the stream has been detached, the stream is in an unusable state.

```php
$stream = $stream->detach();
```

### getSize

Get the size of the stream if known.

```php
$size = $stream->getSize();
```

### tell

Returns the current position of the file read/write pointer.

```php
$pointer = $stream->tell();
```

### eof

Returns true if the stream is at the end of the stream.

```php
$eof = $stream->eof();
```

### isSeekable

Returns whether or not the stream is seekable.

```php
$is_seekable = $stream->isSeekable();
```

### seek

Seek to a position in the stream.

```php
$stream->seek($offset, $whence = SEEK_SET);
```

### rewind

Seek to the beginning of the stream.
If the stream is not seekable, this method will raise an exception;
otherwise, it will perform a seek(0).

```php
$stream->rewind();
```

### isWritable

Returns whether or not the stream is writable.

```php
$is_writable = $stream->isWritable();
```

### write

Write data to the stream.

```php
$bytes_written = $stream->write($string);
```

### isReadable

Returns whether or not the stream is readable.

```php
$is_readable = $stream->isReadable();
```

### read

Read data from the stream.

```php
$read_string = $stream->read($length);
```

### getContents

Returns the remaining contents in a string.

```php
$content = $stream->getContents();
```

### getMetadata

Get stream metadata as an associative array or retrieve a specific key.
The keys returned are identical to the keys returned from PHP's stream_get_meta_data() function.

```php
$content = $stream->getMetadata($key = null);
```

---

## Stream Extension

### withPipe

Return an instance with pipe stream.

```php
$stream = $stream->withPipe($command, $mode);
```

### withFile

Return an instance with file stream.

```php
$stream = $stream->withFile($filename, $mode);
```

### withTemp

Return an instance with temporary stream.

```php
$stream = $stream->withTemp($mode);
```

### withMemory

Return an instance with memory stream.

```php
$stream = $stream->withMemory($mode);
```

### getResource

Retrieve resource of the stream.

```php
$resource = $stream->getResource();
```

### getUri

Retrieve uri of the stream metadata.

```php
$uri = $stream->getUri();
```
