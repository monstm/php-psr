# ServerRequest

Representation of an incoming, server-side HTTP request.

Per the HTTP specification, this interface includes properties for each of the following:
* Protocol version
* HTTP method
* URI
* Headers
* Message body

Additionally,
it encapsulates all data as it has arrived to the application from the CGI and/or PHP environment,
including:
* The values represented in $_SERVER.
* Any cookies provided (generally via $_COOKIE)
* Query string arguments (generally via $_GET, or as parsed via parse_str())
* Upload files, if any (as represented by $_FILES)
* Deserialized body parameters (generally from $_POST)

$_SERVER values MUST be treated as immutable, as they represent application state at the time of request;
as such, no methods are provided to allow modification of those values.
The other values provide such methods, as they can be restored from $_SERVER or the request body,
and may need treatment during the application (e.g., body parameters may be deserialized based on content type).

Additionally,
this interface recognizes the utility of introspecting a request to derive and match additional parameters
(e.g., via URI path matching, decrypting cookie values, deserializing non-form-encoded body content,
matching authorization headers to users, etc).
These parameters are stored in an "attributes" property.

Requests are considered immutable;
all methods that might change state MUST be implemented
such that they retain the internal state of the current message and return an instance that contains the changed state.

---

## ServerRequest Instance

Simple ServerRequest Implementation.

```php
$server_request = new \Samy\Psr7\ServerRequest();
```

---

## ServerRequest Interface

Describes ServerRequest interface.

### getServerParams

Retrieve server parameters.

Retrieves data related to the incoming request environment, typically derived from PHP's $_SERVER superglobal.
The data IS NOT REQUIRED to originate from $_SERVER.

```php
$server_params = $server_request->getServerParams();
```

### getCookieParams

Retrieve cookies.

Retrieves cookies sent by the client to the server.

The data MUST be compatible with the structure of the $_COOKIE superglobal.

```php
$cookie_params = $server_request->getCookieParams();
```

### withCookieParams

Return an instance with the specified cookies.

The data IS NOT REQUIRED to come from the $_COOKIE superglobal, but MUST be compatible with the structure of $_COOKIE.
Typically, this data will be injected at instantiation.

This method MUST NOT update the related Cookie header of the request instance, nor related values in the server params.

This method MUST be implemented in such a way as to retain the immutability of the message,
and MUST return an instance that has the updated cookie values.

```php
$server_request = $server_request->withCookieParams($cookies);
```

### getQueryParams

Retrieve query string arguments.

Retrieves the deserialized query string arguments, if any.

Note: the query params might not be in sync with the URI or server params.
If you need to ensure you are only getting the original values,
you may need to parse the query string from `getUri()->getQuery()` or from the `QUERY_STRING` server param.

```php
$query_params = $server_request->getQueryParams();
```

### withQueryParams

Return an instance with the specified query string arguments.

These values SHOULD remain immutable over the course of the incoming request.
They MAY be injected during instantiation, such as from PHP's $_GET superglobal,
or MAY be derived from some other value such as the URI.
In cases where the arguments are parsed from the URI,
the data MUST be compatible with what PHP's parse_str() would return
for purposes of how duplicate query parameters are handled,
and how nested sets are handled.

Setting query string arguments MUST NOT change the URI stored by the request,
nor the values in the server params.

This method MUST be implemented in such a way as to retain the immutability of the message,
and MUST return an instance that has the updated query string arguments.

```php
$server_request = $server_request->withQueryParams($query);
```

### getUploadedFiles

Retrieve normalized file upload data.

This method returns upload metadata in a normalized tree,
with each leaf an instance of Psr\Http\Message\UploadedFileInterface.

These values MAY be prepared from $_FILES or the message body during instantiation,
or MAY be injected via withUploadedFiles().

```php
$uploaded_files = $server_request->getUploadedFiles();
```

### withUploadedFiles

Create a new instance with the specified uploaded files.

This method MUST be implemented in such a way as to retain the immutability of the message,
and MUST return an instance that has the updated body parameters.

```php
$server_request = $server_request->withUploadedFiles($uploadedFiles);
```

### getParsedBody

Retrieve any parameters provided in the request body.

If the request Content-Type is either application/x-www-form-urlencoded or multipart/form-data,
and the request method is POST, this method MUST return the contents of $_POST.

Otherwise, this method may return any results of deserializing the request body content;
as parsing returns structured content, the potential types MUST be arrays or objects only.
A null value indicates the absence of body content.

```php
$parsed_body = $server_request->getParsedBody();
```

### withParsedBody

Return an instance with the specified body parameters.

These MAY be injected during instantiation.

If the request Content-Type is either application/x-www-form-urlencoded or multipart/form-data,
and the request method is POST, use this method ONLY to inject the contents of $_POST.

The data IS NOT REQUIRED to come from $_POST, but MUST be the results of deserializing the request body content.
Deserialization/parsing returns structured data, and, as such, this method ONLY accepts arrays or objects,
or a null value if nothing was available to parse.

As an example, if content negotiation determines that the request data is a JSON payload,
this method could be used to create a request instance with the deserialized parameters.

This method MUST be implemented in such a way as to retain the immutability of the message,
and MUST return an instance that has the updated body parameters.

```php
$server_request = $server_request->withParsedBody($data);
```

### getAttributes

Retrieve attributes derived from the request.

The request "attributes" may be used to allow injection of any parameters derived from the request:
e.g., the results of path match operations;
the results of decrypting cookies;
the results of deserializing non-form-encoded message bodies;
etc. Attributes will be application and request specific, and CAN be mutable.

```php
$attributes = $server_request->getAttributes();
```

### getAttribute

Retrieve a single derived request attribute.

Retrieves a single derived request attribute as described in getAttributes().
If the attribute has not been previously set, returns the default value as provided.

This method obviates the need for a hasAttribute() method,
as it allows specifying a default value to return if the attribute is not found.

```php
$attributes = $server_request->getAttribute($name, $default = null);
```

### withAttribute

Return an instance with the specified derived request attribute.

This method allows setting a single derived request attribute as described in getAttributes().

This method MUST be implemented in such a way as to retain the immutability of the message,
and MUST return an instance that has the updated attribute.

```php
$server_request = $server_request->withAttribute($name, $value);
```

### withoutAttribute

Return an instance that removes the specified derived request attribute.

This method allows removing a single derived request attribute as described in getAttributes().

This method MUST be implemented in such a way as to retain the immutability of the message,
and MUST return an instance that removes the attribute.

```php
$server_request = $server_request->withoutAttribute($name);
```

---

## ServerRequest Extension

### getParsedFiles

Retrieve any provided file upload data.

```php
$parsed_files = $message->getParsedFiles($name);
```

### hasAttribute

Check if the attribute is exists.

```php
$has_attribute = $message->hasAttribute($name);
```
