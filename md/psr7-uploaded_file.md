# UploadedFile

Value object representing a file uploaded through an HTTP request.

Instances of this interface are considered immutable;
all methods that might change state MUST be implemented such that
they retain the internal state of the current instance and return an instance that contains the changed state.

---

## UploadedFile Instance

Simple UploadedFile Implementation.

```php
$uploaded_file = new \Samy\Psr7\UploadedFile();
```

---

## UploadedFile Interface

Describes Stream interface.

### getStream

Retrieve a stream representing the uploaded file.

This method MUST return a StreamInterface instance, representing the uploaded file.
The purpose of this method is to allow utilizing native PHP stream functionality to manipulate the file upload,
such as stream_copy_to_stream()
(though the result will need to be decorated in a native PHP stream wrapper to work with such functions).

If the moveTo() method has been called previously, this method MUST raise an exception.

```php
$stream = $uploaded_file->getStream();
```

### moveTo

Move the uploaded file to a new location.

Use this method as an alternative to move_uploaded_file().
This method is guaranteed to work in both SAPI and non-SAPI environments.
Implementations must determine which environment they are in,
and use the appropriate method (move_uploaded_file(), rename(), or a stream operation) to perform the operation.

$targetPath may be an absolute path, or a relative path. If it is a relative path,
resolution should be the same as used by PHP's rename() function.

The original file or stream MUST be removed on completion.

If this method is called more than once, any subsequent calls MUST raise an exception.

When used in an SAPI environment where $_FILES is populated, when writing files via moveTo(),
[is_uploaded_file()](http://php.net/is_uploaded_file)
and [move_uploaded_file()](http://php.net/move_uploaded_file)
SHOULD be used to ensure permissions and upload status are verified correctly.

If you wish to move to a stream, use getStream(), as SAPI operations cannot guarantee writing to stream destinations.

```php
$uploaded_file->moveTo($target_path);
```

### getSize

Retrieve the file size.

Implementations SHOULD return the value stored in the "size" key of the file in the $_FILES array if available,
as PHP calculates this based on the actual size transmitted.

```php
$size = $uploaded_file->getSize();
```

### getError

Retrieve the error associated with the uploaded file.

The return value MUST be one of PHP's UPLOAD_ERR_XXX constants.

If the file was uploaded successfully, this method MUST return UPLOAD_ERR_OK.

Implementations SHOULD return the value stored in the "error" key of the file in the $_FILES array.

[Error Messages Explained](http://php.net/manual/en/features.file-upload.errors.php)

```php
$error = $uploaded_file->getError();
```

### getClientFilename

Retrieve the filename sent by the client.

Do not trust the value returned by this method.
A client could send a malicious filename with the intention to corrupt or hack your application.

Implementations SHOULD return the value stored in the "name" key of the file in the $_FILES array.

```php
$client_filename = $uploaded_file->getClientFilename();
```

### getClientMediaType

Retrieve the media type sent by the client.

Do not trust the value returned by this method.
A client could send a malicious media type with the intention to corrupt or hack your application.

Implementations SHOULD return the value stored in the "type" key of the file in the $_FILES array.

```php
$client_media_type = $uploaded_file->getClientMediaType();
```

---

## UploadedFile Extension

### getMd5

Retrieve md5 uploaded file.

```php
$md5 = $uploaded_file->getMd5();
```

### getSha1

Retrieve sha1 uploaded file.

```php
$sha1 = $uploaded_file->getSha1();
```

### getMimeType

Retrieve uploaded file mimetype.

```php
$mimetype = $uploaded_file->getMimeType();
```

### getExtension

Retrieve uploaded file extension.

```php
$extension = $uploaded_file->getExtension();
```
