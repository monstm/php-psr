# Response

Representation of an outgoing, server-side response.

Per the HTTP specification, this interface includes properties for each of the following:

* Protocol version
* Status code and reason phrase
* Headers
* Message body

Responses are considered immutable;
all methods that might change state MUST be implemented such that they retain the internal state of the current message
and return an instance that contains the changed state.

---

## Response Instance

Simple Response Implementation.

```php
$response = new \Samy\Psr7\Response();
```

---

## Response Interface

Describes Response interface.

### getStatusCode

Gets the response status code.

The status code is a 3-digit integer result code of the server's attempt to understand and satisfy the request.

```php
$status_code = $response->getStatusCode();
```

### getReasonPhrase

Gets the response reason phrase associated with the status code.

Because a reason phrase is not a required element in a response status line,
the reason phrase value MAY be empty.
Implementations MAY choose to return the default RFC 7231 recommended reason phrase
(or those listed in the IANA HTTP Status Code Registry) for the response's status code.

```php
$reason_phrase = $response->getReasonPhrase();
```

### withStatus

Return an instance with the specified status code and, optionally, reason phrase.

If no reason phrase is specified,
implementations MAY choose to default to the RFC 7231 or IANA recommended reason phrase for the response's status code.

This method MUST be implemented in such a way as to retain the immutability of the message,
and MUST return an instance that has the updated status and reason phrase.

```php
$response = $response->withStatus($code, $reasonPhrase = "");
```
