# Logger

This is a simple Logger implementation that other Loggers can inherit from.
It simply delegates all log-level-specific methods to the `log` method to
reduce boilerplate code that a simple Logger that does the same thing with
messages regardless of the error level has to implement.

---

## Logger Instance

Simple Logger Implementation.

```php
$logger = new \Samy\Psr3\Logger();
```

---

## Logger Interface

Describes Logger interface.

### emergency

System is unusable.

```php
$logger->emergency($message, $context);
```

### alert

Action must be taken immediately.
Example: Entire website down, database unavailable, etc. This should trigger the SMS alerts and wake you up.

```php
$logger->alert($message, $context);
```

### critical

Critical conditions.
Example: Application component unavailable, unexpected exception.

```php
$logger->critical($message, $context);
```

### error

Runtime errors that do not require immediate action but should typically be logged and monitored.

```php
$logger->error($message, $context);
```

### warning

Exceptional occurrences that are not errors.
Example: Use of deprecated APIs, poor use of an API, undesirable things that are not necessarily wrong.

```php
$logger->warning($message, $context);
```

### notice

Normal but significant events.

```php
$logger->notice($message, $context);
```

### info

Interesting events.
Example: User logs in, SQL logs.

```php
$logger->info($message, $context);
```

### debug

Detailed debug information.

```php
$logger->debug($message, $context);
```

### log

Logs with an arbitrary level.

```php
$logger->log($level, $message, $context);
```
