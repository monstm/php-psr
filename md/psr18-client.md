# Client

---

## Client Instance

Simple Client Implementation.

```php
$client = new \Samy\Psr18\Client();
```

---

## Client Interface

### sendRequest

Sends a PSR-7 request and returns a PSR-7 response.

```php
$response = $client->sendRequest($request);
```
