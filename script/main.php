<?php

require(dirname(__DIR__) . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php");
require("consts.php");
require("functions.php");

statusDataProvider(STATUS_FILE, STATUS_RESULT, STATUS_UNITTEST);
mimetypeDataProvider(MIMETYPE_FILE, MIMETYPE_RESULT);
