<?php

define("ROOT_PATH", dirname(__DIR__));
define("CSV_PATH", ROOT_PATH . DIRECTORY_SEPARATOR . "script" . DIRECTORY_SEPARATOR . "csv");
define("CSV_UNITTEST", ROOT_PATH . DIRECTORY_SEPARATOR . "test" . DIRECTORY_SEPARATOR . "csv");
define("TXT_PATH", ROOT_PATH . DIRECTORY_SEPARATOR . "script" . DIRECTORY_SEPARATOR . "txt");
define("DATA_PATH", ROOT_PATH . DIRECTORY_SEPARATOR . "data-provider");

// https://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
define("STATUS_FILE", CSV_PATH . DIRECTORY_SEPARATOR . "http-status-codes-1.csv");
define("STATUS_RESULT", DATA_PATH . DIRECTORY_SEPARATOR . "status.json");
define("STATUS_UNITTEST", CSV_UNITTEST . DIRECTORY_SEPARATOR . "status.csv");
define("STATUS_UNITTEST_ID", 0);
define("STATUS_UNITTEST_EXCEPTION", 1);
define("STATUS_UNITTEST_CODE", 2);
define("STATUS_UNITTEST_REASON", 3);

// https://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types
define("MIMETYPE_FILE", TXT_PATH . DIRECTORY_SEPARATOR . "mime.types.txt");
define("MIMETYPE_RESULT", DATA_PATH . DIRECTORY_SEPARATOR . "mimetype.json");
