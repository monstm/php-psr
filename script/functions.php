<?php

use Samy\DataProvider\DataProvider;

/**
 * @param string $CsvFilename The csv input filename
 * @param string $ResultFilename The json output filename
 * @param string $UnitTestFilename The unittest output filename
 * @return void
 */
function statusDataProvider(string $CsvFilename, string $ResultFilename, string $UnitTestFilename): void
{
    $result = [];
    foreach (DataProvider::csv($CsvFilename) as $status) {
        $code = $status["Value"] ?? "";
        if (is_numeric($code)) {
            $result[$code] = $status["Description"] ?? "";
        }
    }

    file_put_contents($ResultFilename, json_encode($result, JSON_PRETTY_PRINT));

    $csv = [["id", "exception", "code", "reason"]];
    for ($status = 100; $status < 1000; $status++) {
        $code = strval($status);
        $data = [];
        $data[STATUS_UNITTEST_ID] = "status-" . $code;
        /** @phpstan-ignore-next-line */
        $data[STATUS_UNITTEST_EXCEPTION] = !isset($result[$code]) ? "true" : "false";
        $data[STATUS_UNITTEST_CODE] = $code;
        /** @phpstan-ignore-next-line */
        $data[STATUS_UNITTEST_REASON] = $result[$code] ?? "";
        array_push($csv, $data);
    }

    $file = fopen($UnitTestFilename, "w");
    if ($file) {
        if (is_array($csv)) {
            foreach ($csv as $fields) {
                fputcsv($file, $fields);
            }
        }

        fclose($file);
    }
}

/**
 * @param string $TxtFilename The txt input filename
 * @param string $ResultFilename The json output filename
 * @return void
 */
function mimetypeDataProvider(string $TxtFilename, string $ResultFilename): void
{
    $result = [];
    foreach (DataProvider::lst($TxtFilename) as $line) {
        $line = trim($line);
        if (empty($line) || (substr($line, 0, 1) == "#")) {
            continue;
        }

        $data = explode("\t", $line, 2);
        if (count($data) < 2) {
            continue;
        }

        $mimetype = trim($data[0]);
        $extensions = explode(" ", trim($data[1]));
        $result[$mimetype] = trim($extensions[0]);
    }

    file_put_contents($ResultFilename, json_encode($result, JSON_PRETTY_PRINT));
}
