<?php

namespace Samy\Psr7;

use Samy\Psr\Abstract\AbstractUploadedFile;
use Samy\Psr\DataProvider\DataProvider;
use Samy\Psr\Interface\UploadedFileInterface;

/**
 * Simple UploadedFile Implementation.
 */
class UploadedFile extends AbstractUploadedFile implements UploadedFileInterface
{
    /** @var string */
    private $md5 = "";

    /** @var string */
    private $sha1 = "";

    /** @var string */
    private $mimetype = "";

    /** @var string */
    private $extension = "";

    /**
     * @param array<string,mixed> $Config The configuration.
     */
    public function __construct(array $Config = [])
    {
        parent::__construct($Config);

        $temp_name = $this->getTempName();
        if (!is_file($temp_name)) {
            return;
        }

        /** @phpstan-ignore-next-line */
        $this->md5 = md5_file($temp_name) ?? "";

        /** @phpstan-ignore-next-line */
        $this->sha1 = sha1_file($temp_name) ?? "";

        /** @phpstan-ignore-next-line */
        $this->mimetype = mime_content_type($temp_name) ?? "";

        $mimetype = DataProvider::json("mimetype");

        /** @phpstan-ignore-next-line */
        $this->extension = $mimetype[$this->mimetype] ?? pathinfo($temp_name, PATHINFO_EXTENSION);
    }

    /**
     * Retrieve md5 uploaded file.
     *
     * @return string
     */
    public function getMd5(): string
    {
        return $this->md5;
    }

    /**
     * Retrieve sha1 uploaded file.
     *
     * @return string
     */
    public function getSha1(): string
    {
        return $this->sha1;
    }

    /**
     * Retrieve uploaded file mimetype.
     *
     * @return string
     */
    public function getMimeType(): string
    {
        return $this->mimetype;
    }

    /**
     * Retrieve uploaded file extension.
     *
     * @return string
     */
    public function getExtension(): string
    {
        return $this->extension;
    }
}
