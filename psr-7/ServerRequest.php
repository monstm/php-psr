<?php

namespace Samy\Psr7;

use Samy\Psr3\Logger;
use Samy\Psr\Abstract\AbstractServerRequest;
use Samy\Psr\Interface\ServerRequestInterface;

/**
 * Simple ServerRequest Implementation.
 */
class ServerRequest extends AbstractServerRequest implements ServerRequestInterface
{
    /**
     * @param array<string,mixed> $Config The configuration.
     */
    public function __construct(array $Config = [])
    {
        parent::__construct($Config);

        if (empty($Config["protocol_version"])) {
            $this->initMessageProtocolVersion();
        }

        if (empty($Config["header"])) {
            $this->initMessageHeader();
        }

        if (empty($Config["body"])) {
            $this->initMessageBody();
        }

        if (empty($Config["method"])) {
            $this->initRequestMethod();
        }

        if (empty($Config["uri"])) {
            $this->initRequestUri();
        }

        if (empty($Config["cookie_params"])) {
            $this->initServerRequestCookieParams();
        }

        if (empty($Config["query_params"])) {
            $this->initServerRequestQueryParams();
        }

        if (empty($Config["uploaded_files"])) {
            $this->initServerRequestUploadedFiles();
        }

        if (empty($Config["parsed_body"])) {
            $this->initServerRequestParsedBody();
        }

        if (empty($Config["attribute"])) {
            $this->initServerRequestAttribute();
        }
    }

    /**
     * Initialize Message protocol version.
     *
     * @return void
     */
    private function initMessageProtocolVersion(): void
    {
        $server_protocol = $_SERVER["SERVER_PROTOCOL"] ?? "HTTP/1.0";
        $this->withProtocolVersion(str_replace("HTTP/", "", $server_protocol));
    }

    /**
     * Initialize Message header.
     *
     * @return void
     */
    private function initMessageHeader(): void
    {
        $header = [];

        $getallheaders = function_exists("getallheaders") ? getallheaders() : [];
        foreach ($getallheaders as $name => $value) {
            $key = strtolower($name);
            $header[$key] = $value;
        }

        foreach ($_SERVER as $name => $value) {
            if (!strtoupper(substr($name, 0, 5)) == "HTTP_") {
                continue;
            }

            $key = strtolower(str_replace("_", "-", substr($name, 5)));
            $header[$key] = $value;
        }

        if (!isset($header["authorization"])) {
            $names = [
                "HTTP_AUTHORIZATION",
                "REDIRECT_HTTP_AUTHORIZATION",
                "REDIRECT_REDIRECT_HTTP_AUTHORIZATION"
            ];

            foreach ($names as $name) {
                if (isset($_SERVER[$name])) {
                    $header["authorization"] = $_SERVER[$name];
                    break;
                }
            }
        }

        foreach ($header as $name => $value) {
            $this->withHeader($name, $value);
        }
    }

    /**
     * Initialize Message body.
     *
     * @return void
     */
    private function initMessageBody(): void
    {
        $content = file_get_contents("php://input");

        if (is_string($content)) {
            $stream = $this->getBody();

            $stream->write($content);
            $stream->rewind();
        }
    }

    /**
     * Initialize Request method.
     *
     * @return void
     */
    private function initRequestMethod(): void
    {
        $this->withMethod($_SERVER["REQUEST_METHOD"] ?? "GET");
    }

    /**
     * Initialize Request uri.
     *
     * @return void
     */
    private function initRequestUri(): void
    {
        $https = $_SERVER["HTTPS"] ?? "off";
        $protocol = $https == "on" ? "https" : "http";
        $http_host = $_SERVER["HTTP_HOST"] ?? "localhost";
        $request_uri = $_SERVER["REQUEST_URI"] ?? "/";
        $url = $protocol . "://" . $http_host . $request_uri;
        $this->withRequestTarget($url);
    }

    /**
     * Initialize ServerRequest cookie params.
     *
     * @return void
     */
    private function initServerRequestCookieParams(): void
    {
        $this->withCookieParams($_COOKIE);
    }

    /**
     * Initialize ServerRequest query params.
     *
     * @return void
     */
    private function initServerRequestQueryParams(): void
    {
        $this->withQueryParams($_GET);
    }


    /**
     * Initialize ServerRequest uploaded files.
     *
     * @return void
     */
    private function initServerRequestUploadedFiles(): void
    {
        $this->withUploadedFiles($this->normalizeUploadedFiles($_FILES));
    }

    /**
     * Normalize uploaded files.
     *
     * @param array<mixed> $Nested Nested uploaded file variable.
     * @return array<mixed>
     */
    private function normalizeUploadedFiles(array $Nested): array
    {
        $ret = [];

        foreach ($Nested as $field => $data) {
            if (!is_array($data)) {
                continue;
            }

            if ($this->isValidUploadedFilesStructure($data)) { // if is valid structur
                $ret[$field] = [];

                if ($this->isUploadedFileStructure($data)) { // if is normalized
                    $ret[$field] = new UploadedFile($data);
                } elseif (is_array($data["tmp_name"])) { // if is nested
                    $nested = [];

                    foreach (array_keys($data["tmp_name"]) as $key) {
                        $nested[$key] = [
                            "tmp_name" => $data["tmp_name"][$key] ?? "",
                            "name" => $data["name"][$key] ?? null,
                            "type" => $data["type"][$key] ?? null,
                            "size" => $data["size"][$key] ?? null,
                            "error" => $data["error"][$key] ?? UPLOAD_ERR_NO_FILE
                        ];
                    }

                    $ret[$field] = $this->normalizeUploadedFiles($nested);
                }
            } else { // if is invalid structur
                $ret[$field] = $this->normalizeUploadedFiles($data);
            }
        }

        return $ret;
    }

    /**
     * Check if valid uploadedfile structure.
     *
     * @param array<string,mixed> $Data HTTP file upload variable.
     * @return bool
     */
    private function isValidUploadedFilesStructure(array $Data): bool
    {
        return isset($Data["tmp_name"]) &&
            isset($Data["name"]) &&
            isset($Data["type"]) &&
            isset($Data["error"]) &&
            isset($Data["size"]);
    }

    /**
     * Check if normalize file structure.
     *
     * @param array<string,mixed> $Data HTTP file upload variable.
     * @return bool
     */
    private function isUploadedFileStructure(array $Data): bool
    {
        return is_string($Data["tmp_name"]) &&
            is_string($Data["name"]) &&
            is_string($Data["type"]) &&
            is_int($Data["size"]) &&
            is_int($Data["error"]);
    }

    /**
     * Initialize ServerRequest parsed body.
     *
     * @return void
     */
    private function initServerRequestParsedBody(): void
    {
        $parsed_body = null;
        $content_type = $this->getContentType();
        $content_types = [
            "application/x-www-form-urlencoded",
            "multipart/form-data"
        ];

        if (($this->getMethod() == "POST") && in_array($content_type, $content_types)) {
            $parsed_body = $_POST;
        } else {
            $content = $this->getBody()->getContents();

            switch ($content_type) {
                case "application/x-www-form-urlencoded":
                    parse_str($content, $parsed_body);
                    break;
                case "application/json":
                    $json = @json_decode($content, true);
                    if ($json) {
                        $parsed_body = $json;
                    } else {
                        $logger = new Logger();
                        $logger->warning(json_last_error_msg());
                    }
                    break;
            }
        }

        if (is_array($parsed_body) || is_object($parsed_body) || is_null($parsed_body)) {
            $this->withParsedBody($parsed_body);
        }
    }

    /**
     * Initialize ServerRequest attribute.
     *
     * @return void
     */
    private function initServerRequestAttribute(): void
    {
        $attributes = array_merge($this->attrAuthorization(), $this->attrClient());
        foreach ($attributes as $name => $value) {
            $this->withAttribute($name, $value);
        }
    }

    /**
     * Set authorization attributes.
     *
     * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Authorization
     * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication
     * @return array<string,mixed>
     */
    private function attrAuthorization(): array
    {
        $ret = [
            "has_authorization" => false,
            "auth_type" => "",
            "auth_credentials" => "",
            "auth_username" => "",
            "auth_password" => ""
        ];

        $authorization = $this->getHeaderLine("authorization");
        if (!empty($authorization)) {
            $ret["has_authorization"] = true;

            $explode = explode(" ", $authorization, 2);
            $ret["auth_type"] = $explode[0];
            $ret["auth_credentials"] = $explode[1] ?? "";

            if (strtolower($ret["auth_type"]) == "basic") {
                $base64_decode = base64_decode($ret["auth_credentials"]);
                $credentials = !empty($base64_decode) ? $base64_decode : $ret["auth_credentials"];

                $explode = explode(":", $credentials);
                $ret["auth_username"] = $explode[0];
                $ret["auth_password"] = $explode[1] ?? "";
            }
        }

        return $ret;
    }

    /**
     * Set client attributes.
     *
     * @return array<string,mixed>
     */
    private function attrClient(): array
    {
        $ret = [
            "user_agent" => $_SERVER["HTTP_USER_AGENT"] ?? "",
            "referrer" => $_SERVER["HTTP_REFERER"] ?? "",
            "remote_ip" => $_SERVER["REMOTE_ADDR"] ?? "0.0.0.0",
            "client_ip" => "0.0.0.0"
        ];

        $client_ip_keys = [
            "HTTP_CF_CONNECTING_IP",
            "HTTP_CLIENT_IP",
            "HTTP_X_FORWARDED",
            "HTTP_X_FORWARDED_FOR",
            "HTTP_FORWARDED",
            "HTTP_FORWARDED_FOR",
            "REMOTE_ADDR"
        ];

        foreach ($client_ip_keys as $key) {
            if (isset($_SERVER[$key])) {
                $ret["client_ip"] = $_SERVER[$key];
                break;
            }
        }

        return $ret;
    }

    /**
     * Retrieve any provided file upload data.
     *
     * @param string $Name Input file name.
     * @return array<mixed>
     */
    public function getParsedFiles(string $Name = ""): array
    {
        $result = [];
        foreach ($this->getUploadedFiles() as $key => $uploaded_files) {
            $result[$key] = is_array($uploaded_files) ? $uploaded_files : [$uploaded_files];
        }

        if (isset($result[$Name])) {
            $ret = $result[$Name];
        } else {
            $ret = ($Name == "" ? $result : []);
        }

        return $ret;
    }

    /**
     * Check if the attribute is exists.
     *
     * @param string $name The attribute name.
     * @return bool
     */
    public function hasAttribute(string $name): bool
    {
        return array_key_exists($name, $this->getAttributes());
    }
}
