<?php

namespace Samy\Psr7;

use Samy\Psr\Abstract\AbstractResponse;

/**
 * Simple Response Implementation.
 */
class Response extends AbstractResponse
{
}
