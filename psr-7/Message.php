<?php

namespace Samy\Psr7;

use Samy\Psr\Abstract\AbstractMessage;
use Samy\Psr\Interface\MessageInterface;

/**
 * This is a simple Message implementation that other Message can inherit from.
 */
abstract class Message extends AbstractMessage implements MessageInterface
{
    /**
     * Retrieve content type.
     *
     * @return string
     */
    public function getContentType(): string
    {
        $result = explode(";", $this->getHeaderLine("content-type"));

        return strtolower(trim($result[0]));
    }

    /**
     * Return an instance with the provided value replacing the specified header content type.
     *
     * @param string $Value The content type.
     * @return static
     */
    public function withContentType(string $Value): self
    {
        return $this->withHeader("content-type", $Value);
    }
}
