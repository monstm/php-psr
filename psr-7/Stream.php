<?php

namespace Samy\Psr7;

use InvalidArgumentException;
use Samy\Psr\Abstract\AbstractStream;
use Samy\Psr\Interface\StreamInterface;

/**
 * Simple Stream Implementation.
 */
class Stream extends AbstractStream implements StreamInterface
{
    private const FSTAT_MODE_FIFO = 0010000;
    private const FSTAT_MODE_FILE = 0100000;
    private const META_MODE_WRITABLE = ["r+", "w", "w+", "a", "a+", "x", "x+", "c", "c+", "rw+", "w+b", "a+b"];
    private const META_MODE_READABLE = ["r", "r+", "w+", "a+", "x+", "c+", "rw", "rw+", "rb", "w+b", "a+b"];

    /**
     * Attach new resource to this object.
     *
     * @param mixed $Resource Resource handle.
     * @throws InvalidArgumentException If invalid.
     * @return static
     */
    protected function attach(mixed $Resource): self
    {
        $this->close();
        if (!is_resource($Resource)) {
            throw new InvalidArgumentException("Invalid resource stream");
        }

        $this->resource = $Resource;

        $stats = fstat($this->resource);
        $mode = $stats["mode"] ?? 0;
        $this->is_pipe = (($mode & self::FSTAT_MODE_FIFO) == self::FSTAT_MODE_FIFO);
        $this->is_file = (($mode & self::FSTAT_MODE_FILE) == self::FSTAT_MODE_FILE);

        $meta = stream_get_meta_data($this->resource);
        $mode = strtolower($meta["mode"]);
        $this->is_seekable = $meta["seekable"];
        $this->is_writable = in_array($mode, self::META_MODE_WRITABLE);
        $this->is_readable = in_array($mode, self::META_MODE_READABLE);
        $this->uri = $meta["uri"];

        return $this;
    }

    /**
     * Return an instance with pipe stream.
     *
     * @param string $Command The command.
     * @param string $Mode The mode. Either 'r' for reading, or 'w' for writing.
     * @throws InvalidArgumentException If invalid.
     * @return static
     */
    public function withPipe(string $Command, string $Mode = "w+"): self
    {
        return $this->attach(@popen($Command, $Mode));
    }

    /**
     * Return an instance with file stream.
     *
     * @param string $Filename The filename.
     * @param string $Mode The mode parameter specifies the type of access you require to the stream.
     * @throws InvalidArgumentException If invalid.
     * @return static
     */
    public function withFile(string $Filename, string $Mode = "w+"): self
    {
        return $this->attach(@fopen($Filename, $Mode));
    }

    /**
     * Return an instance with temporary stream.
     *
     * @param string $Mode The mode parameter specifies the type of access you require to the stream.
     * @throws InvalidArgumentException If invalid.
     * @return static
     */
    public function withTemp(string $Mode = "w+"): self
    {
        return $this->withFile("php://temp", $Mode);
    }

    /**
     * Return an instance with memory stream.
     *
     * @param string $Mode The mode parameter specifies the type of access you require to the stream.
     * @throws InvalidArgumentException If invalid.
     * @return static
     */
    public function withMemory(string $Mode = "w+"): self
    {
        return $this->withFile("php://memory", $Mode);
    }

    /**
     * Retrieve resource of the stream.
     *
     * @return ?object
     */
    public function getResource(): ?object
    {
        /** @phpstan-ignore-next-line */
        return $this->resource;
    }

    /**
     * Retrieve uri of the stream metadata.
     *
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }
}
