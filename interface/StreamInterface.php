<?php

namespace Samy\Psr\Interface;

use Psr\Http\Message\StreamInterface as MessageStreamInterface;

/**
 * Describes Stream interface.
 */
interface StreamInterface extends MessageStreamInterface
{
    /**
     * Return an instance with pipe stream.
     *
     * @param string $Command The command.
     * @param string $Mode The mode. Either 'r' for reading, or 'w' for writing.
     * @return static
     */
    public function withPipe(string $Command, string $Mode = "w+"): self;

    /**
     * Return an instance with file stream.
     *
     * @param string $Filename The filename.
     * @param string $Mode The mode parameter specifies the type of access you require to the stream.
     * @return static
     */
    public function withFile(string $Filename, string $Mode = "w+"): self;

    /**
     * Return an instance with temporary stream.
     *
     * @param string $Mode The mode parameter specifies the type of access you require to the stream.
     * @return static
     */
    public function withTemp(string $Mode = "w+"): self;

    /**
     * Return an instance with memory stream.
     *
     * @param string $Mode The mode parameter specifies the type of access you require to the stream.
     * @return static
     */
    public function withMemory(string $Mode = "w+"): self;

    /**
     * Retrieve resource of the stream.
     *
     * @return ?object
     */
    public function getResource(): ?object;

    /**
     * Retrieve uri of the stream metadata.
     *
     * @return string
     */
    public function getUri(): string;
}
