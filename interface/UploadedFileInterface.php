<?php

namespace Samy\Psr\Interface;

use Psr\Http\Message\UploadedFileInterface as MessageUploadedFileInterface;

/**
 * Describes UploadedFile interface.
 */
interface UploadedFileInterface extends MessageUploadedFileInterface
{
    /**
     * Retrieve md5 uploaded file.
     *
     * @return string
     */
    public function getMd5(): string;

    /**
     * Retrieve sha1 uploaded file.
     *
     * @return string
     */
    public function getSha1(): string;

    /**
     * Retrieve uploaded file mimetype.
     *
     * @return string
     */
    public function getMimeType(): string;

    /**
     * Retrieve uploaded file extension.
     *
     * @return string
     */
    public function getExtension(): string;
}
