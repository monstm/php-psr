<?php

namespace Samy\Psr\Interface;

use Psr\Http\Message\ServerRequestInterface as MessageServerRequestInterface;

/**
 * Describes RequestInterface interface.
 */
interface ServerRequestInterface extends MessageServerRequestInterface
{
    /**
     * Retrieve any provided file upload data.
     *
     * @param string $Name Input file name.
     * @return array<mixed>
     */
    public function getParsedFiles(string $Name = ""): array;

    /**
     * Check if the attribute is exists.
     *
     * @param string $name The attribute name.
     * @return bool
     */
    public function hasAttribute(string $name): bool;
}
