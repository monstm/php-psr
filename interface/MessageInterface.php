<?php

namespace Samy\Psr\Interface;

use Psr\Http\Message\MessageInterface as MessageMessageInterface;

/**
 * Describes Message interface.
 */
interface MessageInterface extends MessageMessageInterface
{
    /**
     * Retrieve content type.
     *
     * @return string
     */
    public function getContentType(): string;

    /**
     * Return an instance with the provided value replacing the specified header content type.
     *
     * @param string $Value The content type.
     * @return static
     */
    public function withContentType(string $Value): self;
}
