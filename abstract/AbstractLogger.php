<?php

namespace Samy\Psr\Abstract;

use Psr\Log\AbstractLogger as LogAbstractLogger;
use Psr\Log\InvalidArgumentException;
use Psr\Log\LogLevel;
use Stringable;

/**
 * This is a simple PSR-3 Logger implementation that other PSR-3 Logger can inherit from.
 */
abstract class AbstractLogger extends LogAbstractLogger
{
    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string|Stringable $message
     * @param array<string,mixed> $context
     * @throws InvalidArgumentException
     * @return void
     */
    public function log($level, string|Stringable $message, array $context = []): void
    {
        $interpolate = $this->interpolate($message, $context);
        error_log($level . ": " . $interpolate);

        switch ($level) {
            case LogLevel::EMERGENCY:
            case LogLevel::ALERT:
            case LogLevel::CRITICAL:
            case LogLevel::ERROR:
                trigger_error($interpolate, E_USER_ERROR);
                // break;
            case LogLevel::WARNING:
                trigger_error($interpolate, E_USER_WARNING);
                break;
            case LogLevel::NOTICE:
                trigger_error($interpolate, E_USER_NOTICE);
                break;
        }
    }

    /**
     * Interpolates context values into the message placeholders.
     *
     * @param string $message
     * @param array<string,mixed> $context
     * @return string
     */
    protected function interpolate(string $message, array $context = [])
    {
        $replace_pairs = [];
        foreach ($context as $key => $data) {
            if (!is_string($key)) {
                continue;
            }

            switch (strtolower(gettype($data))) {
                case "boolean":
                    $value = ($data ? "true" : "false");
                    break;
                case "string":
                    $value = $data;
                    break;
                case "array":
                    $encode = json_encode($data);
                    $value = (is_string($encode) ? $encode : "[]");
                    break;
                default:
                    $value = strval($data);
                    break;
            }

            $replace_pairs["{" . $key . "}"] = $value;
        }

        return strtr($message, $replace_pairs);
    }
}
