<?php

namespace Samy\Psr\Abstract;

use Exception;
use Psr\Http\Message\StreamInterface;
use RuntimeException;
use Samy\Psr3\Logger;

/**
 * This is a simple PSR-7 Stream implementation that other PSR-7 Stream can inherit from.
 */
abstract class AbstractStream implements StreamInterface
{
    /** @var ?resource */
    protected $resource = null;

    /** @var bool */
    protected $is_pipe = false;

    /** @var bool */
    protected $is_file = false;

    /** @var bool */
    protected $is_seekable = false;

    /** @var bool */
    protected $is_writable = false;

    /** @var bool */
    protected $is_readable = false;

    /** @var string */
    protected $uri = "";

    /**
     * @param array<string,mixed> $Config The configuration.
     */
    public function __construct(array $Config = [])
    {
        /** @phpstan-ignore-next-line */
        $this->resource = $Config["resource"] ?? null;

        /** @phpstan-ignore-next-line */
        $this->is_pipe = $Config["is_pipe"] ?? false;

        /** @phpstan-ignore-next-line */
        $this->is_file = $Config["is_file"] ?? false;

        /** @phpstan-ignore-next-line */
        $this->is_seekable = $Config["is_seekable"] ?? false;

        /** @phpstan-ignore-next-line */
        $this->is_writable = $Config["is_writable"] ?? false;

        /** @phpstan-ignore-next-line */
        $this->is_readable = $Config["is_readable"] ?? false;

        /** @phpstan-ignore-next-line */
        $this->uri = $Config["is_readable"] ?? "";
    }

    public function __destruct()
    {
        $this->close();
    }

    /**
     * Reads all data from the stream into a string, from the beginning to end.
     *
     * This method MUST attempt to seek to the beginning of the stream before
     * reading data and read the stream until the end is reached.
     *
     * Warning: This could attempt to load a large amount of data into memory.
     *
     * This method MUST NOT raise an exception in order to conform with PHP's
     * string casting operations.
     *
     * @see http://php.net/manual/en/language.oop5.magic.php#object.tostring
     * @return string
     */
    public function __toString(): string
    {
        $ret = "";

        try {
            $ret = $this
                ->rewind()
                ->getContents();
        } catch (Exception $exception) {
            $logger = new Logger();
            $logger->warning($exception->getMessage());
        }

        return $ret;
    }

    /**
     * Closes the stream and any underlying resources.
     *
     * @return void
     */
    public function close(): void
    {
        if (!is_resource($this->resource)) {
            return;
        }

        if ($this->is_pipe) {
            pclose($this->resource);
        }

        if ($this->is_file) {
            fclose($this->resource);
        }

        $this->resource = null;

        $this->is_pipe = false;
        $this->is_file = false;

        $this->is_seekable = false;
        $this->is_writable = false;
        $this->is_readable = false;
        $this->uri = "";
    }

    /**
     * Separates any underlying resources from the stream.
     *
     * After the stream has been detached, the stream is in an unusable state.
     *
     * @return ?resource Underlying PHP stream, if any
     */
    public function detach()
    {
        $ret = $this->resource;
        $this->is_pipe = false;
        $this->is_file = false;
        $this->close();

        return $ret;
    }

    /**
     * Get the size of the stream if known.
     *
     * @return int|null Returns the size in bytes if known, or null if unknown.
     */
    public function getSize(): ?int
    {
        $stats = fstat($this->resource);
        return $stats["size"] ?? null;
    }

    /**
     * Returns the current position of the file read/write pointer.
     *
     * @return int Position of the file pointer
     * @throws RuntimeException on error.
     */
    public function tell(): int
    {
        if (!$this->is_seekable) {
            throw new RuntimeException("Unable to get the position of the pointer in stream.");
        }

        /** @phpstan-ignore-next-line */
        return ftell($this->resource);
    }

    /**
     * Returns true if the stream is at the end of the stream.
     *
     * @return bool
     */
    public function eof(): bool
    {
        return feof($this->resource);
    }

    /**
     * Returns whether or not the stream is seekable.
     *
     * @return bool
     */
    public function isSeekable(): bool
    {
        return $this->is_seekable;
    }

    /**
     * Seek to a position in the stream.
     *
     * @link http://www.php.net/manual/en/function.fseek.php
     * @param int $offset Stream offset
     * @param int $whence Specifies how the cursor position will be calculated
     *     based on the seek offset. Valid values are identical to the built-in
     *     PHP $whence values for `fseek()`.  SEEK_SET: Set position equal to
     *     offset bytes SEEK_CUR: Set position to current location plus offset
     *     SEEK_END: Set position to end-of-stream plus offset.
     * @throws RuntimeException on failure.
     */
    public function seek($offset, $whence = SEEK_SET): self
    {
        if (!$this->is_seekable) {
            throw new RuntimeException("Unable to rewind in stream");
        }

        rewind($this->resource);

        return $this;
    }

    /**
     * Seek to the beginning of the stream.
     *
     * If the stream is not seekable, this method will raise an exception;
     * otherwise, it will perform a seek(0).
     *
     * @see seek()
     * @link http://www.php.net/manual/en/function.fseek.php
     * @throws RuntimeException on failure.
     */
    public function rewind(): self
    {
        if (!$this->is_seekable) {
            throw new RuntimeException("Unable to rewind in stream");
        }

        rewind($this->resource);

        return $this;
    }

    /**
     * Returns whether or not the stream is writable.
     *
     * @return bool
     */
    public function isWritable(): bool
    {
        return $this->is_writable;
    }

    /**
     * Write data to the stream.
     *
     * @param string $string The string that is to be written.
     * @return int Returns the number of bytes written to the stream.
     * @throws RuntimeException on failure.
     */
    public function write($string): int
    {
        if (!$this->is_writable) {
            throw new RuntimeException("Unable to write in stream");
        }

        /** @phpstan-ignore-next-line */
        return fwrite($this->resource, $string);
    }

    /**
     * Returns whether or not the stream is readable.
     *
     * @return bool
     */
    public function isReadable(): bool
    {
        return $this->is_readable;
    }

    /**
     * Read data from the stream.
     *
     * @param int $length Read up to $length bytes from the object and return
     *     them. Fewer than $length bytes may be returned if underlying stream
     *     call returns fewer bytes.
     * @return string Returns the data read from the stream, or an empty string
     *     if no bytes are available.
     * @throws RuntimeException if an error occurs.
     */
    public function read($length): string
    {
        if (!$this->is_readable) {
            throw new RuntimeException("Unable to read in stream");
        }

        /** @phpstan-ignore-next-line */
        return fread($this->resource, $length > 0 ? $length : 0);
    }

    /**
     * Returns the remaining contents in a string.
     *
     * @return string
     * @throws RuntimeException if unable to read or an error occurs while
     *     reading.
     */
    public function getContents(): string
    {
        if (!$this->is_readable) {
            throw new RuntimeException("Unable to read or an error occurs in stream");
        }

        /** @phpstan-ignore-next-line */
        return stream_get_contents($this->resource);
    }

    /**
     * Get stream metadata as an associative array or retrieve a specific key.
     *
     * The keys returned are identical to the keys returned from PHP's
     * stream_get_meta_data() function.
     *
     * @link http://php.net/manual/en/function.stream-get-meta-data.php
     * @param string $key Specific metadata to retrieve.
     * @return array|mixed|null Returns an associative array if no key is
     *     provided. Returns a specific key value if a key is provided and the
     *     value is found, or null if the key is not found.
     */
    public function getMetadata($key = null): mixed
    {
        $ret = null;
        $meta = stream_get_meta_data($this->resource);

        if (is_null($key)) {
            $ret = $meta;
        } elseif (isset($meta[$key])) {
            $ret = $meta[$key];
        }

        return $ret;
    }
}
