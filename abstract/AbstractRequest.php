<?php

namespace Samy\Psr\Abstract;

use InvalidArgumentException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriInterface;
use Samy\Psr7\Message;
use Samy\Psr7\Uri;

/**
 * This is a simple PSR-7 Request implementation that other PSR-7 Request can inherit from.
 */
abstract class AbstractRequest extends Message implements RequestInterface
{
    /** @var string */
    private $method = "";

    /** @var UriInterface */
    private $uri = null;

    /**
     * @param array<string,mixed> $Config The configuration.
     */
    public function __construct(array $Config = [])
    {
        parent::__construct($Config);

        /** @phpstan-ignore-next-line */
        $this->method = $Config["method"] ?? "";

        /** @phpstan-ignore-next-line */
        $this->uri = $Config["uri"] ?? new Uri();
    }

    /**
     * Retrieves the message's request target.
     *
     * Retrieves the message's request-target either as it will appear (for
     * clients), as it appeared at request (for servers), or as it was
     * specified for the instance (see withRequestTarget()).
     *
     * In most cases, this will be the origin-form of the composed URI,
     * unless a value was provided to the concrete implementation (see
     * withRequestTarget() below).
     *
     * If no URI is available, and no request-target has been specifically
     * provided, this method MUST return the string "/".
     *
     * @return string
     */
    public function getRequestTarget(): string
    {
        return $this->uri->__toString();
    }

    /**
     * Return an instance with the specific request-target.
     *
     * If the request needs a non-origin-form request-target — e.g., for
     * specifying an absolute-form, authority-form, or asterisk-form —
     * this method may be used to create an instance with the specified
     * request-target, verbatim.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * changed request target.
     *
     * @link http://tools.ietf.org/html/rfc7230#section-5.3 (for the various
     *     request-target forms allowed in request messages)
     * @param mixed $requestTarget
     * @return static
     */
    public function withRequestTarget($requestTarget): self
    {
        if (!is_string($requestTarget)) {
            return $this;
        }

        $parse_url = parse_url($requestTarget);

        if (isset($parse_url["scheme"])) {
            $this->uri->withScheme($parse_url["scheme"]);
        }

        if (isset($parse_url["user"]) || isset($parse_url["pass"])) {
            $this->uri->withUserInfo(($parse_url["user"] ?? ""), ($parse_url["pass"] ?? null));
        }

        if (isset($parse_url["host"])) {
            $this->uri->withHost($parse_url["host"]);
        }

        if (isset($parse_url["port"])) {
            $this->uri->withPort($parse_url["port"]);
        }

        if (isset($parse_url["path"])) {
            $this->uri->withPath($parse_url["path"]);
        }

        if (isset($parse_url["query"])) {
            $this->uri->withQuery($parse_url["query"]);
        }

        if (isset($parse_url["fragment"])) {
            $this->uri->withFragment($parse_url["fragment"]);
        }

        return $this;
    }

    /**
     * Retrieves the HTTP method of the request.
     *
     * @return string Returns the request method.
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Return an instance with the provided HTTP method.
     *
     * While HTTP method names are typically all uppercase characters, HTTP
     * method names are case-sensitive and thus implementations SHOULD NOT
     * modify the given string.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * changed request method.
     *
     * @param string $method Case-sensitive method.
     * @return static
     * @throws InvalidArgumentException for invalid HTTP methods.
     */
    public function withMethod($method): self
    {
        if (!in_array($method, ["GET", "HEAD", "POST", "PUT", "DELETE", "CONNECT", "OPTIONS", "TRACE", "PATCH"])) {
            throw new InvalidArgumentException("Unsupported method");
        }

        $this->method = $method;

        return $this;
    }

    /**
     * Retrieves the URI instance.
     *
     * This method MUST return a UriInterface instance.
     *
     * @link http://tools.ietf.org/html/rfc3986#section-4.3
     * @return UriInterface Returns a UriInterface instance
     *     representing the URI of the request.
     */
    public function getUri(): UriInterface
    {
        return $this->uri;
    }

    /**
     * Returns an instance with the provided URI.
     *
     * This method MUST update the Host header of the returned request by
     * default if the URI contains a host component. If the URI does not
     * contain a host component, any pre-existing Host header MUST be carried
     * over to the returned request.
     *
     * You can opt-in to preserving the original state of the Host header by
     * setting `$preserveHost` to `true`. When `$preserveHost` is set to
     * `true`, this method interacts with the Host header in the following ways:
     *
     * - If the Host header is missing or empty, and the new URI contains
     *   a host component, this method MUST update the Host header in the returned
     *   request.
     * - If the Host header is missing or empty, and the new URI does not contain a
     *   host component, this method MUST NOT update the Host header in the returned
     *   request.
     * - If a Host header is present and non-empty, this method MUST NOT update
     *   the Host header in the returned request.
     *
     * This method MUST be implemented in such a way as to retain the
     * immutability of the message, and MUST return an instance that has the
     * new UriInterface instance.
     *
     * @link http://tools.ietf.org/html/rfc3986#section-4.3
     * @param UriInterface $uri New request URI to use.
     * @param bool $preserveHost Preserve the original state of the Host header.
     * @return static
     */
    public function withUri(UriInterface $uri, $preserveHost = false): self
    {
        $host = $uri->getHost();
        $this->uri = $uri;

        if ($preserveHost) {
            $this->uri->withHost($host);
        }

        return $this;
    }
}
