<?php

namespace Samy\Psr\Abstract;

use InvalidArgumentException;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UploadedFileInterface;
use RuntimeException;
use Samy\Psr7\Stream;

/**
 * This is a simple PSR-7 UploadedFile implementation that other PSR-7 UploadedFile can inherit from.
 */
abstract class AbstractUploadedFile implements UploadedFileInterface
{
    /** @var string */
    private $tmp_name = "";

    /** @var ?string */
    private $name = null;

    /** @var ?string */
    private $type = null;

    /** @var ?int */
    private $size = null;

    /** @var int */
    private $error = UPLOAD_ERR_NO_FILE;

    /**
     * @param array<string,mixed> $Config The configuration.
     */
    public function __construct(array $Config = [])
    {
        /** @phpstan-ignore-next-line */
        $this->tmp_name = $Config["tmp_name"] ?? "";

        /** @phpstan-ignore-next-line */
        $this->name = $Config["name"] ?? null;

        /** @phpstan-ignore-next-line */
        $this->type = $Config["type"] ?? null;

        /** @phpstan-ignore-next-line */
        $this->size = $Config["size"] ?? null;

        /** @phpstan-ignore-next-line */
        $this->error = $Config["error"] ?? UPLOAD_ERR_NO_FILE;
    }

    /**
     * Retrieve the uploaded tmp_name.
     *
     * @return string
     */
    protected function getTempName(): string
    {
        return $this->tmp_name;
    }

    /**
     * Retrieve a stream representing the uploaded file.
     *
     * This method MUST return a StreamInterface instance, representing the
     * uploaded file. The purpose of this method is to allow utilizing native PHP
     * stream functionality to manipulate the file upload, such as
     * stream_copy_to_stream() (though the result will need to be decorated in a
     * native PHP stream wrapper to work with such functions).
     *
     * If the moveTo() method has been called previously, this method MUST raise
     * an exception.
     *
     * @return StreamInterface Stream representation of the uploaded file.
     * @throws RuntimeException in cases when no stream is available or can be
     *     created.
     */
    public function getStream(): StreamInterface
    {
        if (!is_file($this->tmp_name)) {
            throw new RuntimeException("No stream available");
        }

        $ret = new Stream();
        $ret->withFile($this->tmp_name);

        return $ret;
    }

    /**
     * Move the uploaded file to a new location.
     *
     * Use this method as an alternative to move_uploaded_file(). This method is
     * guaranteed to work in both SAPI and non-SAPI environments.
     * Implementations must determine which environment they are in, and use the
     * appropriate method (move_uploaded_file(), rename(), or a stream
     * operation) to perform the operation.
     *
     * $targetPath may be an absolute path, or a relative path. If it is a
     * relative path, resolution should be the same as used by PHP's rename()
     * function.
     *
     * The original file or stream MUST be removed on completion.
     *
     * If this method is called more than once, any subsequent calls MUST raise
     * an exception.
     *
     * When used in an SAPI environment where $_FILES is populated, when writing
     * files via moveTo(), is_uploaded_file() and move_uploaded_file() SHOULD be
     * used to ensure permissions and upload status are verified correctly.
     *
     * If you wish to move to a stream, use getStream(), as SAPI operations
     * cannot guarantee writing to stream destinations.
     *
     * @see http://php.net/is_uploaded_file
     * @see http://php.net/move_uploaded_file
     * @param string $targetPath Path to which to move the uploaded file.
     * @throws InvalidArgumentException if the $targetPath specified is invalid.
     * @throws RuntimeException on any error during the move operation, or on
     *     the second or subsequent call to the method.
     */
    public function moveTo($targetPath): self
    {
        if (!is_dir(dirname($targetPath))) {
            throw new InvalidArgumentException("Invalid target path");
        }

        if (!is_file($this->tmp_name)) {
            throw new RuntimeException("Subsequent calling method");
        }

        if (!is_uploaded_file($this->tmp_name)) {
            throw new RuntimeException("Invalid uploaded file");
        }

        if (!move_uploaded_file($this->tmp_name, $targetPath)) {
            throw new RuntimeException("Error during the move operation");
        }

        return $this;
    }

    /**
     * Retrieve the file size.
     *
     * Implementations SHOULD return the value stored in the "size" key of
     * the file in the $_FILES array if available, as PHP calculates this based
     * on the actual size transmitted.
     *
     * @return int|null The file size in bytes or null if unknown.
     */
    public function getSize(): ?int
    {
        return $this->size;
    }

    /**
     * Retrieve the error associated with the uploaded file.
     *
     * The return value MUST be one of PHP's UPLOAD_ERR_XXX constants.
     *
     * If the file was uploaded successfully, this method MUST return
     * UPLOAD_ERR_OK.
     *
     * Implementations SHOULD return the value stored in the "error" key of
     * the file in the $_FILES array.
     *
     * @see http://php.net/manual/en/features.file-upload.errors.php
     * @return int One of PHP's UPLOAD_ERR_XXX constants.
     */
    public function getError(): int
    {
        return $this->error;
    }

    /**
     * Retrieve the filename sent by the client.
     *
     * Do not trust the value returned by this method. A client could send
     * a malicious filename with the intention to corrupt or hack your
     * application.
     *
     * Implementations SHOULD return the value stored in the "name" key of
     * the file in the $_FILES array.
     *
     * @return string|null The filename sent by the client or null if none
     *     was provided.
     */
    public function getClientFilename(): ?string
    {
        return $this->name;
    }

    /**
     * Retrieve the media type sent by the client.
     *
     * Do not trust the value returned by this method. A client could send
     * a malicious media type with the intention to corrupt or hack your
     * application.
     *
     * Implementations SHOULD return the value stored in the "type" key of
     * the file in the $_FILES array.
     *
     * @return string|null The media type sent by the client or null if none
     *     was provided.
     */
    public function getClientMediaType(): ?string
    {
        return $this->type;
    }
}
