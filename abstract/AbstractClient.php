<?php

namespace Samy\Psr\Abstract;

use Exception;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Samy\Psr18\NetworkException;
use Samy\Psr18\RequestException;
use Samy\Psr7\Response;
use Samy\Psr\DataTransferObject\CurlResponseDTO;

/**
 * This is a simple PSR-18 Client implementation that other PSR-18 Client can inherit from.
 */
abstract class AbstractClient implements ClientInterface
{
    /** @var ResponseInterface */
    private $response = null;

    /** @var bool */
    protected $follow_location = true;

    /** @var bool */
    protected $ssl_verification = true;

    /** @var int */
    protected $connect_timeout = 3;

    /** @var int */
    protected $timeout = 10;

    /**
     * @param array<string,mixed> $Config The configuration.
     */
    public function __construct(array $Config = [])
    {
        /** @phpstan-ignore-next-line */
        $this->response = $Config["response"] ?? new Response();

        /** @phpstan-ignore-next-line */
        $this->follow_location = $Config["follow_location"] ?? true;

        /** @phpstan-ignore-next-line */
        $this->ssl_verification = $Config["ssl_verification"] ?? true;

        /** @phpstan-ignore-next-line */
        $this->connect_timeout = $Config["connect_timeout"] ?? 3;

        /** @phpstan-ignore-next-line */
        $this->timeout = $Config["timeout"] ?? 30;
    }

    /**
     * Sends a PSR-7 request and returns a PSR-7 response.
     *
     * @param RequestInterface $request
     * @throws ClientExceptionInterface If an error happens while processing the request.
     * @return Response
     */
    public function sendRequest(RequestInterface $request): Response
    {
        $this->response = new Response();

        try {
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_HEADER => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => $this->follow_location,
                CURLOPT_SSL_VERIFYPEER => $this->ssl_verification,
                CURLOPT_CONNECTTIMEOUT => $this->connect_timeout,
                CURLOPT_TIMEOUT => $this->timeout,
                CURLOPT_URL => $request->getRequestTarget(),
                CURLOPT_CUSTOMREQUEST => $request->getMethod(),
                CURLOPT_HTTPHEADER => $this->getRequestHeaders($request),
                CURLOPT_POSTFIELDS => $this->getRequestContent($request)
            ]);

            $curl_result = curl_exec($curl);
            $curl_info = curl_getinfo($curl);
            $error_code = curl_errno($curl);
            $error_message = curl_error($curl);
            curl_close($curl);

            if ($error_code) {
                throw new RequestException($request, $error_message, $error_code);
            }

            $curl_response = new CurlResponseDTO($curl_result, $curl_info);

            $this->response
                ->withStatus($curl_response->status())
                ->withProtocolVersion($curl_response->protocolVersion());

            foreach ($curl_response->header() as $name => $values) {
                $this->response->withHeader($name, $values);
            }

            $stream = $this->response->getBody();
            $stream->write($curl_response->body());
            $stream->rewind();
        } catch (Exception $exception) {
            throw new NetworkException($request, $exception->getMessage(), $exception->getCode(), $exception);
        }

        return $this->response;
    }

    /**
     * Retrieve request headers.
     *
     * @param RequestInterface $request
     * @return array<string>
     */
    private function getRequestHeaders(RequestInterface $request): array
    {
        $ret = [];

        foreach ($request->getHeaders() as $name => $values) {
            array_push($ret, $name . ": " . implode(", ", $values));
        }

        if (!$request->hasHeader("host")) {
            array_push($ret, "Host: " . $request->getUri()->getHost());
        }

        return $ret;
    }

    /**
     * Retrieve request content.
     *
     * @param RequestInterface $request
     * @return string
     */
    private function getRequestContent(RequestInterface $request): string
    {
        $body = $request->getBody();
        $body->rewind();

        return $body->getContents();
    }
}
