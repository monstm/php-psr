<?php

namespace Samy\Psr\DataTransferObject;

use Samy\Validation\Validation;
use Samy\Validation\ValidationException;

class CurlResponseDTO
{
    /** @var int */
    private $status = 0;

    /** @var string */
    private $protocol_version = "";

    /** @var array<string,string> */
    private $header = [];

    /** @var string */
    private $body = "";

    /**
     * @param string|bool $Result The curl result
     * @param array<string,mixed> $Info The curl info
     * @throws ValidationException If invalid.
     */
    public function __construct(string|bool $Result, array $Info)
    {
        $validation = new Validation();
        $validation
            ->withRule("http_code", ["required" => true, "type" => "integer"])
            ->withRule("header_size", ["required" => true, "type" => "integer"])
            ->withRule("content_type", ["required" => true, "type" => "string"])
            ->validate($Info);

        $this->status = is_int($Info["http_code"]) ? $Info["http_code"] : 0;

        if (is_string($Result) && is_int($Info["header_size"])) {
            $header_size = $Info["header_size"];
            $header_lines = explode("\n", substr($Result, 0, $header_size));
            $this->body = substr($Result, $header_size);

            foreach ($header_lines as $line) {
                $line = trim($line);
                if (empty($line)) {
                    continue;
                }

                if (substr($line, 0, 5) == "HTTP/") {
                    $explode = explode(" ", $line);
                    $this->protocol_version = trim(substr($explode[0], 5));
                } elseif (str_contains($line, ":")) {
                    $explode = explode(":", $line, 2);
                    $key = $explode[0];
                    $value = $explode[1] ?? "";
                    $this->header[$key] = trim($value);
                }
            }
        }

        if (is_string($Info["content_type"])) {
            $this->header["content-type"] = $Info["content_type"];
        }
    }

    /**
     * @return int
     */
    public function status(): int
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function protocolVersion(): string
    {
        return $this->protocol_version;
    }

    /**
     * @return array<string,string>
     */
    public function header(): array
    {
        return $this->header;
    }

    /**
     * @return string
     */
    public function body(): string
    {
        return $this->body;
    }
}
